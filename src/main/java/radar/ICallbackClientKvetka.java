package radar;

import radar.protocol.amplitudeTimeDiagram.AmplitudeTimeDiagramResponse;
import radar.protocol.getFPP.GetFPPResponse;
import radar.protocol.getSpectrum.GetSpectrumResponse;
import radar.protocol.setAttAndAmpl.SetAttAndAmpl;
import radar.protocol.setMode.SetMode;
import radar.protocol.setThresholdRI.SetThresholdRI;
import radar.protocol.fpsStateUpdate.EventFPSStateUpdate;
import radar.protocol.updateStateFHRS.EventUpdateStateFHRS;
import radar.protocol.updatingStateRFFRS.EventUpdatingStateRFFRS;

public interface ICallbackClientKvetka {
    void connect();
    void disconnect();
    void changeMode(SetMode response);
    void getSpectrum(GetSpectrumResponse response);
    void getAmplitudeTimeDiagram(AmplitudeTimeDiagramResponse response);
    void getFPSStateUpdate(EventFPSStateUpdate response);
    void setAttAndAmpl(SetAttAndAmpl response);
    void setThresholdRI(SetThresholdRI response);
    void updateStateFHRS(EventUpdateStateFHRS response);
    void updateStateRFFRS(EventUpdatingStateRFFRS response);
    void getFPP(GetFPPResponse response);
}
