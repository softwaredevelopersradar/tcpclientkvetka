package radar.enums;

public enum ECycleSign {
    control((byte)0),
    radiation((byte)1);

    private final byte value;

    ECycleSign(byte b) {
        value = b;
    }

    public byte getValue() {
        return value;
    }

    public static ECycleSign fromByte(byte type) {
        for (ECycleSign val : ECycleSign.values()) {
            if (val.value == type) {
                return val;
            }
        }
        return null;
    }
}
