package radar.enums;

public enum ESpectrumMode {
    once((byte)1),
    periodically((byte)2);

    private final byte value;

    ESpectrumMode(byte b) {
        value = b;
    }

    public byte getValue(){
        return value;
    }

    public static ESpectrumMode fromByte(byte type) {
        for (ESpectrumMode val : ESpectrumMode.values()) {
            if (val.value == type) {
                return val;
            }
        }
        return null;
    }
}
