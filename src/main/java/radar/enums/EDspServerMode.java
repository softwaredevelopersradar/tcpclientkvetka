package radar.enums;

public enum EDspServerMode {
    stop((byte)0),
    radioIntelligence((byte)1),
    radioJammingFrs((byte)2),
    radioJammingFhss((byte)3);

    private final byte value;

    EDspServerMode(byte value) {
        this.value = value;
    }

    public byte getValue(){
        return this.value;
    }

    public static EDspServerMode fromByte(byte type) {
        for (EDspServerMode val : EDspServerMode.values()) {
            if (val.value == type) {
                return val;
            }
        }
        return null;
    }
}
