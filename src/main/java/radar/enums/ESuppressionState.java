package radar.enums;

public enum ESuppressionState {
    no((byte)0),
    suppressed((byte)1);

    private final byte value;

    ESuppressionState(byte b) {
        value = b;
    }

    public byte getValue() {
        return value;
    }

    public static ESuppressionState fromByte(byte type) {
        for (ESuppressionState val : ESuppressionState.values()) {
            if (val.value == type) {
                return val;
            }
        }
        return null;
    }
}
