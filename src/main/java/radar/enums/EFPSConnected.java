package radar.enums;

public enum EFPSConnected {
    connected((byte)0),
    disconnected((byte)1);

    private final byte value;

    EFPSConnected(byte b) {
        value = b;
    }

    public byte getValue() {
        return value;
    }

    public static EFPSConnected fromByte(byte type) {
        for (EFPSConnected val : EFPSConnected.values()) {
            if (val.value == type) {
                return val;
            }
        }
        return null;
    }
}
