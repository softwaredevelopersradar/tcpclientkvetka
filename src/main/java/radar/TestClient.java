package radar;

import radar.enums.ESpectrumMode;
import radar.protocol.amplitudeTimeDiagram.AmplitudeTimeDiagramResponse;
import radar.protocol.fpsStateUpdate.EventFPSStateUpdate;
import radar.protocol.getFPP.GetFPPResponse;
import radar.protocol.getSpectrum.GetSpectrumResponse;
import radar.protocol.setAttAndAmpl.SetAttAndAmpl;
import radar.protocol.setMode.SetMode;
import radar.protocol.setThresholdRI.SetThresholdRI;
import radar.protocol.updateStateFHRS.EventUpdateStateFHRS;
import radar.protocol.updatingStateRFFRS.EventUpdatingStateRFFRS;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class TestClient implements ICallbackClientKvetka {

    public TestClient() {
    }

    public void run()  throws IOException {

        TcpClient tcpClient = new TcpClient( this);

        BufferedReader reader = new BufferedReader(
                new InputStreamReader(System.in));

        while (true)
        {
            System.out.println("Connect - 0");
            System.out.println("Send Change Mode - 1");
            System.out.println("Send Get Spectrum - 2");
            System.out.println("Send Set Th - 3");
            System.out.println("set att and ampl - 4");
            System.out.println("amplitude and time - 5");
            System.out.println("get FPP- 6");
            System.out.println("disconnect - 7");
            System.out.println("Exit - 8");
            String input = reader.readLine();

            switch (Integer.parseInt(input)){
                case 0:
                    tcpClient.startConnection("127.0.0.1", 20003);

                    break;
                case 1:
                    System.out.println("Enter EDspServerMode 0-3");
                    input =  reader.readLine();
                    tcpClient.sendMode(radar.enums.EDspServerMode.fromByte(Byte.parseByte(input)));
                    break;
                case 2:
                    System.out.println("Enter Issuance sign");
                    input =  reader.readLine();
                    radar.enums.ESpectrumMode issuanceSign = radar.enums.ESpectrumMode.fromByte(Byte.parseByte(input));
                    System  .out.println("Enter start frequency");
                    input = reader.readLine();
                    int startFreq = Integer.parseInt(input);
                    System.out.println("Enter end frequency");
                    input = reader.readLine();
                    int endFreq = Integer.parseInt(input);
                    System.out.println("Enter Hz");
                    input = reader.readLine();
                    int hz = Integer.parseInt(input);

                    tcpClient.sendSpectrum(issuanceSign, startFreq, endFreq, hz);
                    break;
                case 3:
                    System.out.println("Enter th");
                    input =  reader.readLine();
                    short th = Short.parseShort(input);

                    tcpClient.sendSetThresholdRI(th);
                    break;

                case 4:
                    System.out.println("Enter Attenuator");
                    input =  reader.readLine();
                    byte attenuator = Byte.parseByte(input);

                    System.out.println("Enter Amplifier");
                    input =  reader.readLine();
                    byte amplifier = Byte.parseByte(input);
                    tcpClient.sendAttAndAmpl(attenuator, amplifier);
                    break;
                case 5:
                    System.out.println("Enter SpectrumMode 1 or 2");
                    input =  reader.readLine();
                    radar.enums.ESpectrumMode mode = ESpectrumMode.fromByte(Byte.parseByte(input));


                    System.out.println("Enter minFreqKHz10");
                    input =  reader.readLine();
                    int minFreqKHz10 = Integer.parseInt(input);

                    System.out.println("Enter maxFreqKHz10");
                    input =  reader.readLine();
                    int maxFreqKHz10 = Integer.parseInt(input);

                    System.out.println("Enter step");
                    input =  reader.readLine();
                    int step = Integer.parseInt(input);

                    System.out.println("Enter numberSlices");
                    input =  reader.readLine();
                    byte numberSlices = Byte.parseByte(input);

                    tcpClient.sendAmplTimeDiagram(mode, minFreqKHz10, maxFreqKHz10, step, numberSlices);
                    break;
                case 6:
                    tcpClient.sendFPP();
                    break;
                case 7:
                    tcpClient.stopConnection();
                    break;
                case 8:
                    return;
            }
        }
    }

    @Override
    public void connect() {
        System.out.println("Connected");
    }

    @Override
    public void disconnect() {
        System.out.println("Disconnected");
    }

    @Override
    public void changeMode(SetMode response) {
        System.out.println(response.toString());

    }

    @Override
    public void getSpectrum(GetSpectrumResponse response) {
        System.out.println(response.toString());
    }

    @Override
    public void getAmplitudeTimeDiagram(AmplitudeTimeDiagramResponse response) {
        System.out.println(response.toString());
    }

    @Override
    public void getFPSStateUpdate(EventFPSStateUpdate response) {
        System.out.println(response.toString());
    }

    @Override
    public void setAttAndAmpl(SetAttAndAmpl response) {
        System.out.println(response.toString());
    }

    @Override
    public void setThresholdRI(SetThresholdRI response) {
        System.out.println(response.toString());
    }

    @Override
    public void updateStateFHRS(EventUpdateStateFHRS response) {
        System.out.println(response.toString());
    }

    @Override
    public void updateStateRFFRS(EventUpdatingStateRFFRS response) {
        System.out.println(response.toString());
    }

    @Override
    public void getFPP(GetFPPResponse response) {
        if(response.getN() != 0)
            System.out.println(response.toString());
        else
            System.out.println("ok, empty");
    }


}