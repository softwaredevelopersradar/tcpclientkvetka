package radar.protocol.getSpectrum;

import radar.Convertor;
import radar.protocol.MessageHeader;

import java.util.Arrays;

public class GetSpectrumResponse {
    private MessageHeader header;
    private byte[] spectrum;

    public GetSpectrumResponse() {

    }

    public GetSpectrumResponse(MessageHeader header, byte[] spectrum) {
        this.header = header;
        this.spectrum = spectrum;
    }

    public GetSpectrumResponse(byte[] data){
        header = new MessageHeader();

        header.setSenderAddress(data[0]);
        header.setReceiverAddress(data[1]);
        header.setCode(data[2]);
        header.setErrorCode(data[3]);
        header.setInformationLength(Convertor.byteArrayToInt32(data,4));


        spectrum = new byte[header.getInformationLength()];
        System.arraycopy(data, 8, spectrum , 0, header.getInformationLength());

    }

    public GetSpectrumResponse(MessageHeader header) {
        this.header = header;
        spectrum = new byte[header.getInformationLength()];
    }


    public MessageHeader getHeader() {
        return header;
    }

    public void setHeader(MessageHeader header) {
        this.header = header;
    }

    public byte[] getSpectrum() {
        return spectrum;
    }

    public void setSpectrum(byte[] spectrum) {
        this.spectrum = spectrum;
    }

    @Override
    public String toString() {
        return "GetSpectrumResponse{" +
                "header=" + header +
                ", spectrum=" + Arrays.toString(spectrum) +
                '}';
    }
}
