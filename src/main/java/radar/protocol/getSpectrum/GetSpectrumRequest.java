package radar.protocol.getSpectrum;

import radar.Convertor;
import radar.enums.ESpectrumMode;
import radar.protocol.MessageHeader;

public class GetSpectrumRequest {
    private MessageHeader header;
    private radar.enums.ESpectrumMode typeSend;
    private int startFrequency;
    private int endFrequency;
    private int freqHZ;

    public GetSpectrumRequest(MessageHeader header, ESpectrumMode typeSend, int startFrequency, int endFrequency, int freqHZ) {
        this.header = header;
        this.typeSend = typeSend;
        this.startFrequency = startFrequency;
        this.endFrequency = endFrequency;
        this.freqHZ = freqHZ;
    }

    public GetSpectrumRequest(byte senderAddress, byte receiverAddress, ESpectrumMode mode, int startFrequency, int endFrequency,
                              int freqHZ) {

        this.typeSend = mode;
        this.startFrequency = startFrequency;
        this.endFrequency = endFrequency;
        this.freqHZ = freqHZ;

        header = new MessageHeader();

        header = new MessageHeader();
        header.setSenderAddress(senderAddress);
        header.setReceiverAddress(receiverAddress);
        header.setCode((byte) 2);
        header.setErrorCode((byte) 0);
        header.setInformationLength(13);
    }

    public byte[] getBytes(){

        byte[] data = new byte[21];

        byte[] bytesMessage = header.getBytes();
        System.arraycopy(bytesMessage, 0, data , 0, 8);

        data[8] = typeSend.getValue();

        byte[] byteStartFrequency = Convertor.getLittleEndian(startFrequency);
        System.arraycopy(byteStartFrequency, 0, data , 9, 4);

        byte[] byteEndFrequency = Convertor.getLittleEndian(endFrequency);
        System.arraycopy(byteEndFrequency, 0, data , 13, 4);

        byte[] byteFreqHZ = Convertor.getLittleEndian(freqHZ);
        System.arraycopy(byteFreqHZ, 0, data , 17, 4);

        return data;
    }


    public MessageHeader getHeader() {
        return header;
    }

    public void setHeader(MessageHeader header) {
        this.header = header;
    }

    public ESpectrumMode getTypeSend() {
        return typeSend;
    }

    public void setTypeSend(ESpectrumMode typeSend) {
        this.typeSend = typeSend;
    }

    public int getStartFrequency() {
        return startFrequency;
    }

    public void setStartFrequency(int startFrequency) {
        this.startFrequency = startFrequency;
    }

    public int getEndFrequency() {
        return endFrequency;
    }

    public void setEndFrequency(int endFrequency) {
        this.endFrequency = endFrequency;
    }

    public int getFreqHZ() {
        return freqHZ;
    }

    public void setFreqHZ(int freqHZ) {
        this.freqHZ = freqHZ;
    }
}
