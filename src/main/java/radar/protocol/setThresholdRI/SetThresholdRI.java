package radar.protocol.setThresholdRI;

import radar.Convertor;
import radar.protocol.MessageHeader;

public class SetThresholdRI {
    private MessageHeader header;
    private short threshold;

    public SetThresholdRI() {
        header = new MessageHeader();
        header.setSenderAddress((byte) 0);
        header.setReceiverAddress((byte) 0);
        header.setCode((byte) 3);
        header.setErrorCode((byte) 0);
        header.setInformationLength(2);

        threshold = 0;
    }

    public SetThresholdRI(MessageHeader header, short threshold) {
        this.header = header;
        this.threshold = threshold;
    }

    public SetThresholdRI(byte idSender, byte idReceiver, short threshold) {
        header = new MessageHeader();
        header.setSenderAddress(idSender);
        header.setReceiverAddress(idReceiver);
        header.setCode((byte) 3);
        header.setErrorCode((byte) 0);
        header.setInformationLength(2);

        this.threshold = threshold;
    }

    public SetThresholdRI(byte[] data) {
        byte[] bytesMessage = new byte[8];
        System.arraycopy(data, 0, bytesMessage, 0, 8);
        header = new MessageHeader(bytesMessage);
        this.threshold = Convertor.byteArrayToShort(data, 8);
    }


    public byte[] getBytes() {
        byte[] data = new byte[10];

        byte[] bytesMessage = header.getBytes();
        System.arraycopy(bytesMessage, 0, data, 0, 8);

        byte[] bytesTh = Convertor.getLittleEndian(threshold);
        System.arraycopy(bytesTh, 0, data, 8, 2);

        return data;
    }

    public MessageHeader getHeader() {
        return header;
    }

    public void setHeader(MessageHeader header) {
        this.header = header;
    }

    public short getThreshold() {
        return threshold;
    }

    public void setThreshold(short threshold) {
        this.threshold = threshold;
    }

    @Override
    public String toString() {
        return "SetThresholdRI{" +
                "header=" + header +
                ", threshold=" + threshold +
                '}';
    }
}
