package radar.protocol.setAttAndAmpl;

import radar.protocol.MessageHeader;

public class SetAttAndAmpl {
    private MessageHeader header;
    private byte attenuator;
    private byte amplifiers;

    public SetAttAndAmpl(){
        header = new MessageHeader();
    }

    public SetAttAndAmpl(MessageHeader header, byte attenuator, byte amplifiers) {
        this.header = header;
        this.attenuator = attenuator;
        this.amplifiers = amplifiers;
    }

    public SetAttAndAmpl(byte idSender, byte idReceiver, byte attenuator, byte amplifiers){
        header = new MessageHeader();

        header.setSenderAddress(idSender);
        header.setReceiverAddress(idReceiver);
        header.setCode((byte) 4);
        header.setErrorCode((byte) 0);
        header.setInformationLength(2);

        this.attenuator = attenuator;
        this.amplifiers = amplifiers;
    }

    public SetAttAndAmpl(byte[] data) {
        byte[] bytesMessage = new byte[8];
        System.arraycopy(data, 0, bytesMessage , 0, 8);
        header = new MessageHeader(bytesMessage);
        this.attenuator = data[8];
        this.amplifiers = data[9];
    }

    public byte[] getBytes(){
        byte[] data = new byte[10];

        byte[] bytesMessage = header.getBytes();
        System.arraycopy(bytesMessage, 0, data , 0, 8);

        data[8] = attenuator;
        data[9] = amplifiers;

        return data;
    }

    public MessageHeader getHeader() {
        return header;
    }

    public void setHeader(MessageHeader header) {
        this.header = header;
    }

    public byte getAttenuator() {
        return attenuator;
    }

    public void setAttenuator(byte attenuator) {
        this.attenuator = attenuator;
    }

    public byte getAmplifiers() {
        return amplifiers;
    }

    public void setAmplifiers(byte amplifiers) {
        this.amplifiers = amplifiers;
    }

    @Override
    public String toString() {
        return "SetAttAndAmpl{" +
                "header=" + header +
                ", attenuator=" + attenuator +
                ", amplifiers=" + amplifiers +
                '}';
    }
}
