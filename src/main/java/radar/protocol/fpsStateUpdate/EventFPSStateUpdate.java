package radar.protocol.fpsStateUpdate;

import radar.Convertor;
import radar.enums.EFPSConnected;
import radar.protocol.MessageHeader;

public class EventFPSStateUpdate {
    private MessageHeader header;
    private radar.enums.EFPSConnected state;

    public EventFPSStateUpdate(MessageHeader header, EFPSConnected state) {
        this.header = header;
        this.state = state;
    }

    public EventFPSStateUpdate(byte[] data) {
        header = new MessageHeader();

        header.setSenderAddress(data[0]);
        header.setReceiverAddress(data[1]);
        header.setCode(data[2]);
        header.setErrorCode(data[3]);
        header.setInformationLength(Convertor.byteArrayToInt32(data, 4));

        state = EFPSConnected.fromByte(data[8]);
    }

    public MessageHeader getHeader() {
        return header;
    }

    public void setHeader(MessageHeader header) {
        this.header = header;
    }

    public EFPSConnected getState() {
        return state;
    }

    public void setState(EFPSConnected state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return "EventFPSStateUpdate{" +
                "header=" + header +
                ", state=" + state +
                '}';
    }
}
