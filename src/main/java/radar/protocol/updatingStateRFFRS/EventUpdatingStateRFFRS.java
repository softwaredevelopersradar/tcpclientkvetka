package radar.protocol.updatingStateRFFRS;

import radar.Convertor;
import radar.enums.ECycleSign;
import radar.protocol.MessageHeader;

import java.util.Arrays;

public class EventUpdatingStateRFFRS {
    private MessageHeader header;
    private radar.enums.ECycleSign state;
    private EventPackage[] packages;


    public EventUpdatingStateRFFRS(MessageHeader header, ECycleSign state, EventPackage[] packages) {
        this.header = header;
        this.state = state;
        this.packages = packages;
    }

    public EventUpdatingStateRFFRS(byte[] data) {
        header = new MessageHeader();

        header.setSenderAddress(data[0]);
        header.setReceiverAddress(data[1]);
        header.setCode(data[2]);
        header.setErrorCode(data[3]);
        header.setInformationLength(Convertor.byteArrayToInt32(data,4));


        state = ECycleSign.fromByte(data[8]);
        int countPackage = (header.getInformationLength()-1)/12;
        packages = new EventPackage[countPackage];

        int startRead = 9;
        for (int i = 0; i < countPackage; i++) {
            packages[i] = new EventPackage();
            packages[i].id = Convertor.byteArrayToInt32(data, startRead);
            packages[i].freqKHz10 = Convertor.byteArrayToInt32(data, startRead + 4);
            packages[i].amplitude = data[startRead + 8];
            packages[i].controlState = data[startRead + 9];
            packages[i].suppressionState = data[startRead + 10];
            packages[i].radiationState = data[startRead + 11];
            startRead += 12;
        }
    }

    public MessageHeader getHeader() {
        return header;
    }

    public void setHeader(MessageHeader header) {
        this.header = header;
    }

    public ECycleSign getState() {
        return state;
    }

    public void setState(ECycleSign state) {
        this.state = state;
    }

    public EventPackage[] getPackages() {
        return packages;
    }

    public void setPackages(EventPackage[] packages) {
        this.packages = packages;
    }

    @Override
    public String toString() {
        return "EventUpdatingStateRFFRS{" +
                "header=" + header +
                ", state=" + state +
                ", packages=" + Arrays.toString(packages) +
                '}';
    }

    private class EventPackage {
        int id;
        int freqKHz10;
        byte amplitude;
        byte controlState;
        byte suppressionState;
        byte radiationState;

        EventPackage() {

        }

        EventPackage(int id, int freqKHz10, byte amplitude, byte controlState, byte suppressionState, byte radiationState) {
            this.id = id;
            this.freqKHz10 = freqKHz10;
            this.amplitude = amplitude;
            this.controlState = controlState;
            this.suppressionState = suppressionState;
            this.radiationState = radiationState;
        }

        @Override
        public String toString() {
            return "EventPackage{" +
                    "id=" + id +
                    ", freqKHz10=" + freqKHz10 +
                    ", amplitude=" + amplitude +
                    ", controlState=" + controlState +
                    ", suppressionState=" + suppressionState +
                    ", radiationState=" + radiationState +
                    '}';
        }
    }
}
