package radar.protocol.updateStateFHRS;

import radar.Convertor;
import radar.enums.ESuppressionState;
import radar.protocol.MessageHeader;

import java.util.Arrays;

public class EventUpdateStateFHRS {
    private MessageHeader header;
    private byte countSuppressedNetworks;
    private EventPackage[] packages;

    public EventUpdateStateFHRS(MessageHeader header, byte countSuppressedNetworks, EventPackage[] packages) {
        this.header = header;
        this.countSuppressedNetworks = countSuppressedNetworks;
        this.packages = packages;
    }

    public EventUpdateStateFHRS(byte[] data) {
        header = new MessageHeader();

        header.setSenderAddress(data[0]);
        header.setReceiverAddress(data[1]);
        header.setCode(data[2]);
        header.setErrorCode(data[3]);
        header.setInformationLength(Convertor.byteArrayToInt32(data,4));

        this.countSuppressedNetworks = data[8];
        packages = new EventPackage[this.countSuppressedNetworks];

        int startRead = 9;

        for (int i = 0; i < this.countSuppressedNetworks; i++) {
            byte state = data[startRead];
            byte id = data[startRead+1];
            byte amplitude = data[startRead+2];
            byte countSuppressedFreq = data[startRead+3];
            int[] freqKHz10 = new int[Convertor.toUnsigned(countSuppressedFreq)];

            for (int j = 0 ; j < countSuppressedFreq; j++)
                freqKHz10[j] = Convertor.byteArrayToInt32(data, (startRead + 4) + 4 * j);

            packages[i] = new EventPackage(state, id, amplitude, countSuppressedFreq, freqKHz10);
            startRead += 4 + (Convertor.toUnsigned(countSuppressedFreq) * 4);
        }
    }

    public MessageHeader getHeader() {
        return header;
    }

    public void setHeader(MessageHeader header) {
        this.header = header;
    }

    public byte getCountSuppressedNetworks() {
        return countSuppressedNetworks;
    }

    public void setCountSuppressedNetworks(byte countSuppressedNetworks) {
        this.countSuppressedNetworks = countSuppressedNetworks;
    }

    public EventPackage[] getPackages() {
        return packages;
    }

    public void setPackages(EventPackage[] packages) {
        this.packages = packages;
    }

    @Override
    public String toString() {
        return "EventUpdateStateFHRS{" +
                "header=" + header +
                ", countSuppressedNetworks=" + countSuppressedNetworks +
                ", packages=" + Arrays.toString(packages) +
                '}';
    }

    private class EventPackage {
        final radar.enums.ESuppressionState state;
        final byte id;
        final byte amplitude;
        final byte countSuppressedFreq;
        int[] freqKHz10;

        EventPackage(byte state, byte id, byte amplitude,byte countSuppressedFreq, int[] freqKHz10){
            this.state = ESuppressionState.fromByte(state);
            this.id = id;
            this.amplitude = amplitude;
            this.countSuppressedFreq = countSuppressedFreq;
            this.freqKHz10 = new int[Convertor.toUnsigned(this.countSuppressedFreq)];
            this.freqKHz10 = freqKHz10;
        }

        @Override
        public String toString() {
            return "EventPackage{" +
                    "state=" + state +
                    ", id=" + id +
                    ", amplitude=" + amplitude +
                    ", countSuppressedFreq=" + countSuppressedFreq +
                    ", freqKHz10=" + Arrays.toString(freqKHz10) +
                    '}';
        }
    }
}
