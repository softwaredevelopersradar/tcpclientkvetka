package radar.protocol.setMode;

import radar.Convertor;
import radar.enums.EDspServerMode;
import radar.protocol.MessageHeader;

import java.io.Serializable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class SetMode implements Serializable {
    private MessageHeader header;
    private radar.enums.EDspServerMode mode;

    public SetMode(){
    }

    public SetMode(MessageHeader header, EDspServerMode mode) {
        this.header = header;
        this.mode = mode;
    }

    public SetMode(byte senderAddress, byte receiverAddress, radar.enums.EDspServerMode mode) {

        header = new MessageHeader(senderAddress, receiverAddress, (byte)1, (byte)0, mode.getValue());

        this.mode = mode;
    }


    public SetMode(byte[] data) {
        header = new MessageHeader();

        header.setSenderAddress(data[0]);
        header.setReceiverAddress(data[1]);
        header.setCode(data[2]);
        header.setErrorCode(data[3]);
        header.setInformationLength(Convertor.byteArrayToInt32(data,4));

        mode = EDspServerMode.fromByte(data[8]);

    }

    public byte[] getBytes() {
        byte[] data = new byte[9];
        data[0] = header.getSenderAddress();
        data[1] = header.getReceiverAddress();
        data[2] = header.getCode();
        data[3] = header.getErrorCode();

        ByteBuffer byteBuffer = ByteBuffer.allocate(4);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
        byteBuffer.putInt(mode.getValue());

        System.arraycopy(byteBuffer.array(), 0, data , 4, 4);

        data[8] = mode.getValue();

        return data;
    }

    public MessageHeader getHeader() {
        return header;
    }

    public void setHeader(MessageHeader header) {
        this.header = header;
    }

    public EDspServerMode getMode() {
        return mode;
    }

    public void setMode(EDspServerMode mode) {
        this.mode = mode;
    }

    @Override
    public String toString() {
        return "SetMode{" +
                "header=" + header +
                ", mode=" + mode +
                '}';
    }
}
