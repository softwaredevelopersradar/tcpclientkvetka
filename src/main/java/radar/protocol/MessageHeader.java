package radar.protocol;

import radar.Convertor;

public class MessageHeader {
    private byte senderAddress;
    private byte receiverAddress;
    private byte code;
    private byte errorCode;
    private int informationLength;

    public  MessageHeader(){
    }

    public MessageHeader(byte senderAddress, byte receiverAddress, byte code, byte errorCode, int informationLength) {
        this.senderAddress = senderAddress;
        this.receiverAddress = receiverAddress;
        this.code = code;
        this.errorCode = errorCode;
        this.informationLength = informationLength;
    }

    public MessageHeader(byte[] data){
        senderAddress = data[0];
        receiverAddress = data[1];
        code = data[2];
        errorCode = data[3];

        informationLength = Convertor.byteArrayToInt32(data,4);
    }

    public byte[] getBytes() {
        byte[] data = new byte[8];
        data[0] = senderAddress;
        data[1] = receiverAddress;
        data[2] = code;
        data[3] = errorCode;

        byte[] byteInformationLength = Convertor.getLittleEndian(informationLength);
        System.arraycopy(byteInformationLength, 0, data , 4, 4);

        return data;
    }

    public void setSenderAddress(byte senderAddress) {
        this.senderAddress = senderAddress;
    }

    public void setReceiverAddress(byte receiverAddress) {
        this.receiverAddress = receiverAddress;
    }

    public void setCode(byte code) {
        this.code = code;
    }

    public void setErrorCode(byte errorCode) {
        this.errorCode = errorCode;
    }

    public void setInformationLength(int informationLength) {
        this.informationLength = informationLength;
    }

    public byte getSenderAddress() {
        return senderAddress;
    }

    public byte getReceiverAddress() {
        return receiverAddress;
    }

    public byte getCode() {
        return code;
    }

    public byte getErrorCode() {
        return errorCode;
    }

    public int getInformationLength() {
        return informationLength;
    }


    @Override
    public String toString() {
        return "MessageHeader{" +
                "senderAddress=" + senderAddress +
                ", receiverAddress=" + receiverAddress +
                ", code=" + code +
                ", errorCode=" + errorCode +
                ", informationLength=" + informationLength +
                '}';
    }

}
