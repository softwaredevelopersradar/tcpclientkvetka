package radar.protocol.getFPP;

import radar.protocol.MessageHeader;

public class GetFPPRequest {
    private MessageHeader header;

    public GetFPPRequest(byte idSender, byte idReceiver) {
        header = new MessageHeader();
        header.setCode((byte) 9);
        header.setErrorCode((byte) 0);
        header.setReceiverAddress(idReceiver);
        header.setSenderAddress(idSender);
        header.setInformationLength(0);
    }

    public byte[] getBytes()
    {
        return header.getBytes();
    }
}
