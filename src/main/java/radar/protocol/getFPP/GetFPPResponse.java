package radar.protocol.getFPP;

import radar.Convertor;
import radar.enums.ESpectrumMode;
import radar.protocol.MessageHeader;
import radar.protocol.amplitudeTimeDiagram.AmplitudeTimeDiagramResponse;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

public class GetFPPResponse {

    private MessageHeader header;
    private float bFreq;
    private float eFreq;
    private float id;
    private float step;
    private int n;
    private float[] levelsArr;
    private float[] azsArr;

    public GetFPPResponse(byte[] data) {
        byte[] bytesMessage = new byte[8];
        System.arraycopy(data, 0, bytesMessage , 0, 8);
        header = new MessageHeader(bytesMessage);

        int dest = 8;
        bFreq = ByteBuffer.wrap(data, dest, 4).order(ByteOrder.LITTLE_ENDIAN).getFloat();
        dest += 4;

        eFreq = ByteBuffer.wrap(data, dest, 4).order(ByteOrder.LITTLE_ENDIAN).getFloat();
        dest += 4;

        id = ByteBuffer.wrap(data, dest, 4).order(ByteOrder.LITTLE_ENDIAN).getFloat();
        dest += 4;

        step = ByteBuffer.wrap(data, dest, 4).order(ByteOrder.LITTLE_ENDIAN).getFloat();
        dest += 4;

        n = Convertor.byteArrayToInt32(data, dest);
        dest += 4;

        levelsArr = new float[n];
        for(int i = 0; i < n; i++) {
            levelsArr[i] = ByteBuffer.wrap(data, dest + i*4, 4).order(ByteOrder.LITTLE_ENDIAN).getFloat();
        }
        dest += 4 * n;

        azsArr = new float[n];
        for(int i = 0; i < n; i++) {
            azsArr[i] = ByteBuffer.wrap(data, dest + i*4, 4).order(ByteOrder.LITTLE_ENDIAN).getFloat();
        }
    }

    public MessageHeader getHeader() {
        return header;
    }

    public float getbFreq() {
        return bFreq;
    }

    public float geteFreq() {
        return eFreq;
    }

    public float getId() {
        return id;
    }

    public float getStep() {
        return step;
    }

    public int getN() {
        return n;
    }

    public float[] getLevelsArr() {
        return levelsArr;
    }

    public float[] getAzsArr() {
        return azsArr;
    }

    public GetFPPResponse(byte idSender, byte idReceiver) {
        header = new MessageHeader();
        header.setCode((byte) 8);
        header.setErrorCode((byte) 0);
        header.setReceiverAddress(idReceiver);
        header.setSenderAddress(idSender);
        header.setInformationLength(0);
    }

    @Override
    public String toString() {
        return "getFPPResponse{" +
                "header=" + header +
                ", bFreq=" + bFreq +
                ", eFreq=" + eFreq +
                ", id=" + id +
                ", step=" + step +
                ", n=" + n +
                ", levelsArr=" + Arrays.toString(levelsArr) +
                ", azsArr=" + Arrays.toString(azsArr) +
                '}';
    }

}
