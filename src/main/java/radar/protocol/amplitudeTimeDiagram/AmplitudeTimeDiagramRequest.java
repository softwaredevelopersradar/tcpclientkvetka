package radar.protocol.amplitudeTimeDiagram;

import radar.Convertor;
import radar.enums.ESpectrumMode;
import radar.protocol.MessageHeader;

public class AmplitudeTimeDiagramRequest {
    private MessageHeader header;
    private radar.enums.ESpectrumMode mode;
    private int minFreqKHz10;
    private int maxFreqKHz10;
    private int step;
    private byte numberSlices;

    public AmplitudeTimeDiagramRequest() {
        header = new MessageHeader();
        mode = radar.enums.ESpectrumMode.once;
        minFreqKHz10 = 0;
        maxFreqKHz10 = 0;
        step = 0;
        numberSlices = 0;
    }

    public AmplitudeTimeDiagramRequest(MessageHeader header, ESpectrumMode mode, int minFreqKHz10, int maxFreqKHz10, int step,
                                       byte numberSlices) {
        this.header = header;
        this.mode = mode;
        this.minFreqKHz10 = minFreqKHz10;
        this.maxFreqKHz10 = maxFreqKHz10;
        this.step = step;
        this.numberSlices = numberSlices;
    }

    public AmplitudeTimeDiagramRequest(byte idSender, byte idReceiver, ESpectrumMode mode, int minFreqKHz10, int maxFreqKHz10,
                                int step, byte numberSlices) {
        header = new MessageHeader();
        header.setCode((byte) 5);
        header.setErrorCode((byte) 0);
        header.setReceiverAddress(idReceiver);
        header.setSenderAddress(idSender);
        header.setInformationLength(14);

        this.mode = mode;
        this.minFreqKHz10 = minFreqKHz10;
        this.maxFreqKHz10 = maxFreqKHz10;
        this.step = step;
        this.numberSlices = numberSlices;
    }

    public byte[] getBytes(){
        byte[] data = new byte[22];

        byte[] bytesMessage = header.getBytes();
        System.arraycopy(bytesMessage, 0, data , 0, 8);

        data[8] = mode.getValue();

        byte[] byteMinFreq = Convertor.getLittleEndian(minFreqKHz10);
        System.arraycopy(byteMinFreq, 0, data , 9, 4);

        byte[] byteMaxFreq = Convertor.getLittleEndian(maxFreqKHz10);
        System.arraycopy(byteMaxFreq, 0, data , 13, 4);

        byte[] bytesStep = Convertor.getLittleEndian(step);
        System.arraycopy(bytesStep, 0, data , 17, 4);

        data[21] = numberSlices;

        return data;
    }
    public MessageHeader getHeader() {
        return header;
    }

    public void setHeader(MessageHeader header) {
        this.header = header;
    }

    public ESpectrumMode getMode() {
        return mode;
    }

    public void setMode(ESpectrumMode mode) {
        this.mode = mode;
    }

    public int getMinFreqKHz10() {
        return minFreqKHz10;
    }

    public void setMinFreqKHz10(int minFreqKHz10) {
        this.minFreqKHz10 = minFreqKHz10;
    }

    public int getMaxFreqKHz10() {
        return maxFreqKHz10;
    }

    public void setMaxFreqKHz10(int maxFreqKHz10) {
        this.maxFreqKHz10 = maxFreqKHz10;
    }

    public int getStep() {
        return step;
    }

    public void setStep(int step) {
        this.step = step;
    }

    public byte getNumberSlices() {
        return numberSlices;
    }

    public void setNumberSlices(byte numberSlices) {
        this.numberSlices = numberSlices;
    }

}
