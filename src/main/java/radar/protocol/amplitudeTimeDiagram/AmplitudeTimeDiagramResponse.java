package radar.protocol.amplitudeTimeDiagram;

import radar.Convertor;
import radar.protocol.MessageHeader;

import java.util.Arrays;

public class AmplitudeTimeDiagramResponse {
    private MessageHeader header;
    private byte countSlices;
    private Slice[] slices;

    public AmplitudeTimeDiagramResponse() {
        header = new MessageHeader();
        countSlices = 0;
    }

    public AmplitudeTimeDiagramResponse(MessageHeader header, byte countSlices, Slice[] slices) {
        this.header = header;
        this.countSlices = countSlices;
        this.slices = slices;
    }

    public AmplitudeTimeDiagramResponse(byte[] data) {
        byte[] bytesMessage = new byte[8];
        System.arraycopy(data, 0, bytesMessage , 0, 8);
        header = new MessageHeader(bytesMessage);

        countSlices = data[8];
        slices = new Slice[Convertor.toUnsigned(countSlices)];

        int startRead = 9;
        for (int i =0; i < countSlices; i++) {
            slices[i] = new Slice(Convertor.byteArrayToShort(data, 9));
            System.arraycopy(data, startRead + 4, slices[i].amplitudes, 0, slices[i].countAmplitudes);
            startRead += 4 + slices[i].countAmplitudes;
        }
    }

    public void setSlices(Slice[] slices) {
        this.slices = slices;
    }

    public MessageHeader getHeader() {
        return header;
    }

    public void setHeader(MessageHeader header) {
        this.header = header;
    }

    public byte getCountSlices() {
        return countSlices;
    }

    public void setCountSlices(byte countSlices) {
        this.countSlices = countSlices;
    }

    public Slice[] getSlices() {
        return slices;
    }

    @Override
    public String toString() {
        return "AmplitudeTimeDiagramResponse{" +
                "header=" + header +
                ", countSlices=" + countSlices +
                ", slices=" + Arrays.toString(slices) +
                '}';
    }

    private class Slice {
        final int countAmplitudes;
        final byte[] amplitudes;

        Slice(short countAmplitudes){
            this.countAmplitudes = countAmplitudes;
            amplitudes = new byte[this.countAmplitudes];
        }

        Slice(byte[] data) {
            countAmplitudes = Convertor.byteArrayToInt32(data, 0);
            amplitudes = new byte[countAmplitudes];
            System.arraycopy(data, 0, amplitudes , 0, countAmplitudes);
        }

        @Override
        public String toString() {
            return "Slice{" +
                    "countAmplitudes=" + countAmplitudes +
                    ", amplitudes=" + Arrays.toString(amplitudes) +
                    '}';
        }
    }
}
