package radar;

import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        TestClient testClient = new TestClient();
        
        try {
            testClient.run();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
