package radar;

import radar.enums.EDspServerMode;
import radar.enums.ESpectrumMode;
import radar.protocol.amplitudeTimeDiagram.AmplitudeTimeDiagramRequest;
import radar.protocol.amplitudeTimeDiagram.AmplitudeTimeDiagramResponse;
import radar.protocol.fpsStateUpdate.EventFPSStateUpdate;
import radar.protocol.getFPP.GetFPPRequest;
import radar.protocol.getFPP.GetFPPResponse;
import radar.protocol.updateStateFHRS.EventUpdateStateFHRS;
import radar.protocol.updatingStateRFFRS.EventUpdatingStateRFFRS;
import radar.protocol.getSpectrum.GetSpectrumRequest;
import radar.protocol.getSpectrum.GetSpectrumResponse;
import radar.protocol.MessageHeader;
import radar.protocol.setMode.SetMode;
import radar.protocol.setAttAndAmpl.SetAttAndAmpl;
import radar.protocol.setThresholdRI.SetThresholdRI;

import java.net.*;
import java.io.*;


public class TcpClient  {
    private final ICallbackClientKvetka cb;

    private Socket clientSocket;
    private DataOutputStream out;
    private DataInputStream  in;

    private Thread threadReceiver;
    private boolean isRunning;

    private final byte idSender;
    private final byte idReceiver;

    private int startFrequency;
    private int endFrequency;

    public int getStartFrequency()
    {
        return  startFrequency;
    }
    public  int getEndFrequency()
    {
        return  endFrequency;
    }


    public TcpClient(ICallbackClientKvetka callback){
        this.idSender = 0;
        this.idReceiver = 0;
        cb = callback;
        isRunning = false;
    }

    public void startConnection(String ip, int port) {
        try {
            clientSocket = new Socket(ip, port);
            out = new DataOutputStream(clientSocket.getOutputStream());
            in = new DataInputStream(clientSocket.getInputStream());
            cb.connect();
            startReceive();
        } catch (IOException e) {
            cb.disconnect();
            e.printStackTrace();
        }
    }

    private void startReceive() {
        isRunning = true;

        Runnable r = () -> {
            while (isRunning) {
                try {
                    byte[] header = new byte[8];
                    byte[] allMessage = new byte[0];
                    byte[] info;

                    in.read(header, 0, 8);
                    MessageHeader messageHeader = new MessageHeader(header);

                    if (messageHeader.getInformationLength() > 0) {
                        int bufferSize = 64;
                        byte[] buffer = new byte[bufferSize];
                        info = new byte[messageHeader.getInformationLength()];
                        int readByte = 0;
                        int read;
                        while (readByte != messageHeader.getInformationLength()) {
                            if(messageHeader.getInformationLength() - readByte < bufferSize)
                                read = in.read(buffer, 0, messageHeader.getInformationLength() - readByte);
                            else
                                read = in.read(buffer);
                            System.arraycopy(buffer, 0, info, readByte, read);
                            readByte += read;
                        }
                        allMessage = Convertor.concat(messageHeader.getBytes(), info);
                    }


                    switch (messageHeader.getCode()) {
                        case 1 -> {
                            SetMode modeMessage = new SetMode(allMessage);
                            cb.changeMode(modeMessage);
                        }
                        case 2 -> {
                            GetSpectrumResponse getSpectrumResponse = new GetSpectrumResponse(allMessage);

                            cb.getSpectrum(getSpectrumResponse);
                        }
                        case 3 -> {
                            SetThresholdRI setThresholdRI = new SetThresholdRI(allMessage);
                            cb.setThresholdRI(setThresholdRI);
                        }
                        case 4 -> {
                            SetAttAndAmpl setAttAndAmpl = new SetAttAndAmpl(allMessage);
                            cb.setAttAndAmpl(setAttAndAmpl);
                        }
                        case 5 -> {
                            AmplitudeTimeDiagramResponse amplitudeTimeDiagramResponse
                                    = new AmplitudeTimeDiagramResponse(allMessage);
                            cb.getAmplitudeTimeDiagram(amplitudeTimeDiagramResponse);
                        }
                        case 6 -> {
                            EventUpdatingStateRFFRS eventUpdateStateFHRS = new EventUpdatingStateRFFRS(allMessage);
                            cb.updateStateRFFRS(eventUpdateStateFHRS);
                        }
                        case 7 -> {
                            EventFPSStateUpdate eventFPSStateUpdate = new EventFPSStateUpdate(allMessage);
                            cb.getFPSStateUpdate(eventFPSStateUpdate);
                        }
                        case 8 -> {
                            EventUpdateStateFHRS eventUpdatingStateRFFRS = new EventUpdateStateFHRS(allMessage);
                            cb.updateStateFHRS(eventUpdatingStateRFFRS);
                        }
                        case 9 ->{
                            GetFPPResponse getFPPResponse = new GetFPPResponse(allMessage);
                            cb.getFPP(getFPPResponse);
                        }
                        default -> {
                        }
                    }
                } catch (IOException e) {
                    stopConnection();
                    e.printStackTrace();
                }
            }
        };

        threadReceiver = new Thread(r, "readerThread");
        threadReceiver.start();
    }

    public void sendMode(EDspServerMode mode) {
        SetMode message = new SetMode(idSender, idReceiver, mode);

        try {
            byte[] a = message.getBytes();
            out.write(a);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sendSpectrum(radar.enums.ESpectrumMode mode, int startFrequency, int endFrequency, int Hz) {
        GetSpectrumRequest getSpectrumRequest =  new GetSpectrumRequest(idSender, idReceiver, mode,
                startFrequency, endFrequency, Hz);
        this.startFrequency = startFrequency;
        this.endFrequency = endFrequency;
        try {
            out.write(getSpectrumRequest.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void sendSetThresholdRI(short th){
        SetThresholdRI setThresholdRIRequest = new SetThresholdRI(idSender, idReceiver, th);
        try {
            out.write(setThresholdRIRequest.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sendAttAndAmpl(byte attenuator, byte amplifier) {
        SetAttAndAmpl attAndAmplRequest = new SetAttAndAmpl(idSender, idReceiver, attenuator, amplifier);
        try {
            out.write(attAndAmplRequest.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sendAmplTimeDiagram(ESpectrumMode mode, int minFreqKHz10, int  maxFreqKHz10, int  step, byte numberSlices){
        AmplitudeTimeDiagramRequest amplitudeTimeDiagramRequest = new AmplitudeTimeDiagramRequest(idSender, idReceiver, mode,
                minFreqKHz10, maxFreqKHz10, step, numberSlices);
        try {
            out.write(amplitudeTimeDiagramRequest.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sendFPP(){
        GetFPPRequest getFPPRequest = new GetFPPRequest(idSender, idReceiver);
        try {
            out.write(getFPPRequest.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void stopConnection() {
        try {
            cb.disconnect();
            isRunning = false;
            threadReceiver.interrupt();
            in.close();
            out.close();
            clientSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
