package radar;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.List;

public class Convertor {

    public static byte[] convertToBytes(Object object) throws IOException {

        try (ByteArrayOutputStream bos = new ByteArrayOutputStream()) {
            ObjectOutputStream out;
            out = new ObjectOutputStream(bos);
            out.writeObject(object);
            out.flush();
            return bos.toByteArray();
        }
    }

    public static Object convertFromBytes(byte[] bytes) throws IOException, ClassNotFoundException {
        ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
        ObjectInput in = null;
        try {
            in = new ObjectInputStream(bis);
            return in.readObject();
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }


    public static byte[] getLittleEndian(int num){
        ByteBuffer byteBuffer = ByteBuffer.allocate(4);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
        byteBuffer.putInt(num);

        return byteBuffer.array();
    }

    public static byte[] getLittleEndian(short num){
        ByteBuffer byteBuffer = ByteBuffer.allocate(2);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
        byteBuffer.putShort(num);

        return byteBuffer.array();
    }

    public static int byteArrayToInt32(byte[] bytes, int dest) {
        return ((bytes[dest] & 0xFF)) |
                ((bytes[dest + 1] & 0xFF) << 8) |
                ((bytes[dest + 2] & 0xFF) << 16) |
                ((bytes[dest + 3] & 0xFF) << 24);
    }

    public static short byteArrayToShort(byte[] bytes, int dest) {
        return (short) (((bytes[dest] & 0xFF)) |
                        ((bytes[dest + 1] & 0xFF) << 8));
    }

    public static byte[] toByteArray(List<Byte> in) {
        final int n = in.size();
        byte[] ret = new byte[n];
        for (int i = 0; i < n; i++) {
            ret[i] = in.get(i);
        }
        return ret;
    }

    public static float byteArrayToFloat(byte[] data, int dest)
    {
        return Float.intBitsToFloat( data[dest] ^ data[dest+1]<<8 ^ data[dest+2]<<16 ^ data[dest+3]<<24 );
    }


    public static byte[] concat(byte[] a, byte[] b) {

        byte[] c = new byte[a.length + b.length];
        System.arraycopy(a, 0, c, 0, a.length);
        System.arraycopy(b, 0, c, a.length, b.length);
        return c;
    }

    public static char toUnsigned(byte b) {
        return (char) (b >= 0 ? b : 256 + b);
    }

}
